# -*- coding: utf-8 -*-
"""
@file Lab4_UI.py
@brief This UI waits for the go and stop commands, and plots the data it receives. 
Created on Fri Oct 23 21:17:24 2020

@author: Alexander Lewis
"""
import time

import serial
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import xlsxwriter



ser = serial.Serial(port='COM7',baudrate=115273,timeout=1)



class Lab4_UI:
    
    S0_waiting = 0
    S1_data_gathering = 1
    S2_plotting = 2
    

    def __init__(self, interval):
        self.state = self.S0_waiting
        
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        self.curr_time = self.start_time
        
        
        self.time = []
        self.position = []
        
    def run(self):
        
        self.curr_time = time.time()
    
        if self.curr_time > self.next_time:
          
            if(self.state == self.S0_waiting):
                print("Waiting to gather data. Enter g or G when you want to start.")
                # Run State 0 Code
                self.time = []
                self.position = []
                Go = self.sendGo()
                if (Go == 2):
                    self.transitionTo(self.S1_data_gathering)
                    print("Gathering data")
                else:
                    pass
                
            
            elif(self.state == self.S1_data_gathering):
                # Run State 1 Code
                Stop = self.sendStop()               
                if (Stop == 2):
                    self.transitionTo(self.S2_plotting)
                else:
                    pass
                
                
                
                            
            elif(self.state == self.S2_plotting):
                #ser.write(str('a').encode('ascii'))
                self.request_data()
                print("Here is the csv file:")
                
                self.transitionTo(self.S0_waiting)
                
                
                
                    
                    
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
            
            


    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

            
    
    def sendGo(self):
        inv = input('Give me a character: ')
        if (str(inv) == 'G') or (str(inv) == 'g'):
            ser.write(str(inv).encode('ascii'))
            return 2
        else: 
            
            return 3
    
    def sendStop(self):
        inv = input('Press s or S to stop collection: ')
        if (str(inv) == 'S') or (str(inv) == 's'):
            ser.write(str(inv).encode('ascii'))
            return 2
        else: 
            
            return 3
        
    def request_data(self):
        raw_data = []
        print('requesting data')
        x = ser.readline().decode('ascii')
        
        while True:
            x = ser.readline().decode('ascii')
            raw_data = x.strip().split(',')
            
            print(raw_data)
            self.time.append(raw_data[0])
            
            if (raw_data[0] == ''):
                break
            else:
                self.position.append(raw_data[1])
        
        print(self.position)
        self.time.pop()
        print("TIME")
        print(self.time)
        fig, ax = plt.subplots()
        ax.plot(self.time, self.position)
        ax.set(xlabel='time', ylabel='position',
        title='Encoder Position vs Time Data')
        ax.grid()

        fig.savefig("Lab4_output_graph.png")
        plt.show()
        
        
        # Creates a workbook and adds a worksheet.
        workbook = xlsxwriter.Workbook('Lab4_output.xlsx')
        worksheet = workbook.add_worksheet()

        # Start from the first cell. Rows and columns are zero indexed.
        row = 0
        col = 0

        # Iterate over the data and write it out row by row.
        for t in (self.time):
            worksheet.write(row, col, t)
            row += 1
        row = 0
        for position in (self.position):
            worksheet.write(row, col + 1, position)
            row += 1

        

        workbook.close()
        
                                     
        

UI = Lab4_UI(.1)
while True:
       
    UI.run()