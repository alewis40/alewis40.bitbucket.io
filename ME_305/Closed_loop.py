# -*- coding: utf-8 -*-
"""
@file Closed_loop.py
@brief This script contains contains the methods used for proportional control.
@date Created on Tue Nov 24 16:11:59 2020

@author: Alexander Lewis
"""

import pyb
import Encoder_driver
import shares
import utime

class ClosedLoop:
    
    def __init__(self,init_kp):
        '''
        @brief initializes loop parameters and encoder
        '''
        
        ## Initial kp
        self._kp = init_kp
        
        self.timer = 4
        self.pin1 = pyb.Pin.cpu.B6
        self.pin2 = pyb.Pin.cpu.B7

        self.encoder = Encoder_driver.Encoder(self.pin1,self.pin2,self.timer,dbg = True)
        
        self._w_actual = 0 
        
        self.start = utime.ticks_ms()
        self.end = self.start
        self.pos1 = 0 
    def update(self):
        '''
        @brief This method calculates the new Vm using w_actual, w_desired, and Kp
        '''
        self.w_actual()
        shares.Vm = self._kp *(shares.des_w - self._w_actual)
        
    
    def w_actual(self):
        '''
        @brief This method calculates the current speed of the motor in rpm
        '''
        self.start = utime.ticks_ms()
        self.pos1 = self.encoder.update()
        pyb.delay(1000)
        pos2 = self.encoder.update()
        #print('pos1 = '+ str(pos1))
        #print('pos2 = '+ str(pos2))
        delta = self.encoder.delta()
        #print('delta: ' + str(delta))
        self.end = utime.ticks_ms()
        
        time_delta = (self.end - self.start)
        
        
        
        ## Speed in rpm
        ## 1_click/4_pulse,1_rotation/1000_clicks , 4.16 gear ratio , 1000_ms/1_s 60_s/1_min
        self._w_actual  = (delta/time_delta)*(1/4)*(1/1000)*1000*(60)/4.16
        print(self._w_actual)
    def get_kp(self):
        '''
        @brief This method returns the current Kp value
        '''
        kp = self._kp
        return kp

    def set_kp(self,new_kp):
        '''
        @brief This method changes the current kp value.
        '''
        self._kp = new_kp

# # test code
# while True:
#     loop = ClosedLoop(1)
#     loop.update()
        

        
        
        