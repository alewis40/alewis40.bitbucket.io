# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 11:12:35 2020

@author: Alexander Lewis
"""
#from Task_User_6.py import Gather_Data
from Lab_6_main import Lab_6_main
from pyb import UART

myuart = UART(2)

Task_nucleo = Lab_6_main()

while True:
    if myuart.any() != 0:
        Task_nucleo.gain = int(myuart.readchar().decode('ascii'))
    Task_nucleo.run()
    
 #   Task_user.run()
    