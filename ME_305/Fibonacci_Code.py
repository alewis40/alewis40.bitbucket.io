"""
#  @file Fibonacci_Code.py
#  @brief This script returns the value of whichever number in the fibonacci sequence is requested.
#  @author Alexander Lewis
# 
#  @details This is the code link: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Fibonacci_Code.py
#  @date September 25, 2020
"""
def fib(index):
    '''@brief A function that finds a given number in the Fibonacci sequence'''
    n = 3
    sequence = [0,1,1] 
    value = 0
    if index < 3:
        output = sequence[index]
    else:
        while n <= index:
            output = sequence[n-2] + sequence[n-1]
            sequence.append(output)
            n = n + 1
    print("The Fibonacci sequence up to your value is: " + str(sequence))
    print("Your value is: " + str(output))

        

if __name__ == '__main__':
    print("Please enter a positive integer.")
    idx = input("Which Fibonaci number do you want? ")
    fib(int(idx))

    

    
    