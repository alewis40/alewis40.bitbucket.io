'''
@file main_Elevator.py
@brief Runs 2 Virtual Elevators
'''

from FSM_elevator import Elevator, MotorDriver, Button

       
# Creating object 1 to pass into task constructor
Button_1a = Button('PB6')
Button_2a = Button('PB7')
Firsta = Button('PB8')
Seconda = Button('PB9')
Motor1a = MotorDriver()

# Creating object 2 to pass into task constructor
Button_1b = Button('PB10')
Button_2b = Button('PB11')
Firstb = Button('PB12')
Secondb = Button('PB13')
Motor1b = MotorDriver()

# Creating a task object using the button, sensor, and motor objects above
task1 = Elevator(0.1, Firsta, Seconda, Button_1a, Button_2a, Motor1a)
task2 = Elevator(0.1, Firstb, Secondb, Button_1b, Button_2b, Motor1b)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    #elevator 1
    task1.run()
    #elevator 2
    task2.run()