# -*- coding: utf-8 -*-
"""
@file Lab_6_main.py
@brief Alternates between the Closed_loop.py and updating the motor speed
Created on Tue Nov 24 16:51:46 2020

@author: Alexander Lewis
"""

import pyb
from Closed_loop import ClosedLoop
from Motor_Driver import Motor
import shares
from pyb import UART
import utime

myuart = UART(2)

class Lab_6_main:
    
    def __init__(self):
        
        ## Set initial Kp
        self.gain = 1
        
        ## Set initial desired speed in rpm
        self.des_w = 100
        
        ## Sets  initial duty cycle %
        shares.des_w = self.des_w
        
        ## Sets up motor
        self.Motor1 = Motor()
        self.Motor1.motor_disable()
            
        # ## Sets up loop
        self.Loop1 = ClosedLoop(self.gain)

        ## Enable motor
        self.Motor1.motor_enable()
        
        
        ## Set Kp
        self.Loop1.set_kp(self.gain)


        self._start_time = utime.ticks_ms()

    def run(self):
        '''@brief This continually updates the error, the duty cycle, and the gain. 
           @detail The data doesn't have to be saved in a list. because each value is being sent continuously, the user interface will read it when it is in the plotting mode. '''
        ## Sets up loop
        Loop1 = ClosedLoop(shares.gain)
        
        ## Adjusts Motor duty percentage
        self.Motor1.motor_set_duty(shares.Vm)
        
        ## Updates the actual speed
        Loop1.update()
        
        ## Calculates error
        error = (self.des_w - self.Loop1._w_actual)/self.des_w
        
        ## Calculates time
        time = utime.ticks_ms() - self._start_time
        
        
        
        
        # currently labeled this way for troubleshooting
        #print('time: ' + str(time))
        #print('error: ' + str(error))
        
        
    

#nucleo = Lab_6_main()


#while True:
#    nucleo.run()

