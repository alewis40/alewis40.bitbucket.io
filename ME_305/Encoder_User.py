'''
@file Encoder_User.py
@brief User interface task as a finite state machine
@details This user interface is run on the user's computer. It waits for a command and then sends the response to the command that it receives from the shares.py file. 
@author Alexander Lewis modified from Charlie Refvem's TaskUser.py File'

'''

import shares
import utime
from pyb import UART

class TaskUser:
    '''
    User interface task.
    
    An object of this class interacts with serial port UART2 waiting for 
    characters. Each character received is checked and if it is an english
    letter a-z it is sent through a cipher in TaskCipher.
    
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_WAIT_FOR_CHAR    = 1
    
    ## Waiting for response state
    S2_WAIT_FOR_RESP    = 2

    def __init__(self, taskNum, interval,dbg=True):
        '''
        Creates a user interface task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        '''
    
        ## The number of the task
        self.taskNum = taskNum
        self.dbg = dbg
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        
        

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            
            if(self.state == self.S0_INIT):
                

                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
                
            
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                    # Run State 1 Code
                
                if self.ser.any():
                    
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                    
                    shares.cmd = self.ser.readchar()
                    
                    
            
            elif(self.state == self.S2_WAIT_FOR_RESP):
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
                # Run State 2 Code
                
                if shares.resp:
        
                    output = shares.resp
                    print(output)
                    shares.resp = None
                    shares.cmd = None
                    
                    
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState