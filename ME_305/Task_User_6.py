# -*- coding: utf-8 -*-
"""
@file Task_User_6.py
@brief This script runs Lab_6_main and its own task. It sends kp to Lab_6_main. 
@date Created on Mon Oct 26 15:47:43 2020
@author: Alexander Lewis
"""
import pyb
import utime
from pyb import UART
import Lab_6_main
from Lab_6_main import Loop1
import shares


class Gather_Data:
    '''
    @brief     A user task class that reads values to update kp or tracks data.
    ''' 
    ## Waiting 
    S1_Waiting = 1
    
    ## Tracking Position
    S2_Tracking   = 2
    
    ## Sending info to UI
    S3_Plotting = 3
    

    
    def __init__(self, interval, tracking_interval, dbg=True):

        ## The state to run on the next iteration of the task.
        self.state = self.S1_Waiting
        self.dbg = dbg
        
        ## Converting entered data tracking interval from seconds to microseconds
        self.tracking_interval = tracking_interval*1000000
                           
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval)     
              
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Sets last_ticks for the first run
        self.last_ticks = 0
        
        ## Sets initial position
        self.position = 0
        
        self.myuart = UART(2)
        
        self.curr_ticks = self.last_ticks
        ## Initialize the lists
        
        self.time_list = []
        self.error_list = []
        self.w_actual_list = []
        self.w_des_list = []
        


        
    def run(self):
        
        '''
        @brief      Runs one iteration of the task
        ''' 
        self.curr_time = utime.ticks_us()
        #print('outside loop')
        if (self.curr_time - self.next_time >= 0):
            #print('inside loop')
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
            ##Run Initialization State
            
            if (self.state == self.S1_Waiting):
            ## Waits for Kp input
                
                if self.myuart.any() != 0:
                    ## Sets Kp
                    shares.gain = int(self.myuart.readline())
                    print(shares.gain)
                    self.myuart.readchar()
                    self.zero_time = utime.ticks_ms()  
                    self.transitionTo(self.S2._Tracking)
                    ##zero the lists
                    self.time_list = []
                    self.error_list = []
                    self.w_actual_list = []
                    self.w_des_list = []
                    
                else:
                    pass

            ##Collects data and waits for stop command  
            
            elif (self.state == self.S2_Tracking):
                
                time = (utime.ticks_ms() - self.zero_time)
                if (time <= time + 30000):
                    # runs for 30 seconds
                    #creates 4 lists
                    self.time_list.append(time)
                    self.error_list.append(Lab_6_main.error)
                    self.w_actual_list.append(Loop1._w_actual)
                    self.w_des_list.append(shares.w_des)
                
                    
                else:
                    self.transitionTo(self.S3_Plotting)
            
            elif (self.state == self.S3_Plotting):
                    
                for i in self.time_list:
                    ## sends time
                    self.myuart.write(str(i) + ',')
                self.myuart.write('/')
                for i in self.error_list:
                    ##sends error
                    self.myuart.write(str(i) + ',')
                self.myuart.write('/')
                for i in self.w_actual_list:
                    ##sends w_actual
                    self.myuart.write(str(i) + ',')
                self.myuart.write('/')
                for i in self.w_des_list:
                    ##sends w_des speed
                    self.myuart.write(str(i) + ',')
                self.myuart.write(None)
                self.transitionTo(self.S1_Waiting)
            else:
                # Invalid state code (error handling)
                pass
        
            
                
                
                
        self.curr_time = utime.ticks_us()        
        

     
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
## This actually runs the main code

nucleo = Lab_6_main()

## This makes data lists and sends them as strings to the UI
usertask = Gather_Data()


while True:
    nucleo.run()
    usertask.run()
    
    
    
    # def UI(self,cmd):
    #     '''
    #     Gives different outputs for different User input commands
    #     '''

            
    #     if (cmd == 100):
    #         resp = self.delta(self.update())
    #         print(resp)
            
    #     elif (cmd == 112):
    #         resp = self.get_position(self.delta(self.update()))
    #         print(resp)
            
    #     elif (cmd == 122):
    #         resp = self.set_position(0)
    #         print(resp)
          
        
    #     else:
            
    #         resp = cmd
            
    #     return resp
    