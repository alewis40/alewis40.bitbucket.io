# -*- coding: utf-8 -*-
"""
Created on Sat Oct 10 22:54:01 2020

@author: Alexander Lewis
"""
import math
import pyb

pinA5 = pyb.Pin (pyb.Pin.cpu.A5) # method
tim1 = pyb.Timer(2, freq = 20000)
t2ch1 = tim1.channel(1,pyb.Timer.PWM,pin=pinA5)


n = 0
for t in range(50000):
    x = 100*math.sin(t*.01)
    pyb.delay(10)
    t2ch1.pulse_width_percent(x)
    