# -*- coding: utf-8 -*-
"""
@file Encoder.py
@brief Tracks and reports encoder position
@details This script tracks the encoder's position, and can return either position, delta, or it can zero the encoder position. 

Created on Mon Oct 19

@author: Alexander Lewis
"""

import shares
import pyb
import utime


class Encoder:
    '''
    @brief      A class to track the position of an encoder.
    '''
    ## Initialization state
    S0_INIT       = 0
    
    ## Tracking Position
    S1_Tracking   = 1
    
    def __init__(self, interval, pin1, pin2, timer,dbg=True):
        '''
        Creates an encoder object
        @param Timer identifying which timer will be used on the Nucleo
        @param pin1 identifies the first encoder pin
        @param pin2 identifies the second encoder pin
        
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S1_Tracking
        self.dbg = dbg
        
        self.timer = timer
        self.pin1 = pin1
        self.pin2 = pin2
        
        ## assigns the timer on the Nucleo
        self.tim = pyb.Timer(timer)
        ## sets the prescaler so that it catches every tick, and sets the period
        self.tim.init(prescaler = 0,period =0xFFFF)
        
        ## sets the channels      
        self.tim.channel(1,pin = self.pin1, mode = pyb.Timer.ENC_AB)
        self.tim.channel(2,pin = self.pin2, mode = pyb.Timer.ENC_AB)
                           
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval)     
              
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Sets last_ticks for the first run
        self.last_ticks = 0
        
        ## Sets initial position
        self.position = 0
        
        
        


        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
                   
        ##
        
        self.curr_time = utime.ticks_us()
        
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            ##Run Initialization State
            if (self.state == self.S0_INIT):
                self.transitionTo(self.S1_WAIT_FOR_CMD)
                
                

            ##Tracks position, then checks for commands    
            elif (self.state == self.S1_Tracking):
                
                curr_ticks = self.update()
                delta = self.delta(curr_ticks)
                position = self.get_position(delta)
                
                if shares.cmd:
                    

                    shares.resp = self.UI(shares.cmd)
                    shares.cmd = None
                    

                    
            else:
                # Invalid state code (error handling)
                pass
            
                
                
                
                
                        # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            self.last_ticks = curr_ticks
            
            
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)

     
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
    
    def delta(self,curr_ticks):
        '''
        Finds the change in ticks from the last iteration
        '''
        delta = curr_ticks - self.last_ticks
        return delta

        
    def update(self):
        '''
        Updates current number of ticks
        '''        
        ticks = self.tim.counter()
        return ticks;
    
    def set_position(self, pos_entered):
        '''
        Enters new position
        '''        
        self.position = pos_entered
        
            

    def get_position(self, Delta):
        '''
        Adds delta to the running position tally
        '''        
        self.position += Delta
        return self.position
    
    def UI(self,cmd):
        '''
        Gives different outputs for different User input commands
        '''

            
        if (cmd == 100):
            resp = self.delta(self.update())
            print(resp)
            
        elif (cmd == 112):
            resp = self.get_position(self.delta(self.update()))
            print(resp)
            
        elif (cmd == 122):
            resp = self.set_position(0)
            print(resp)
          
        
        else:
            
            resp = cmd
            
        return resp

    
            
        
        
        
        