# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 00:43:30 2020

@author: Alexander Lewis
"""

import pyb
from Motor_Driver import Motor

## Set control pin
pinA15 = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)

## PWM pin 1
pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
## timer
tim3 = pyb.Timer(3,freq = 20000)

## Channel 1 
t3ch1 = tim3.channel(3,pyb.Timer.PWM,pin=pinB0)

## Set channel 1 to zero
t3ch1.pulse_width_percent(0)     

## PWM pin 2   
pinB1 = pyb.Pin(pyb.Pin.cpu.B1)
## Channel 2
t3ch2 = tim3.channel(4,pyb.Timer.PWM,pin = pinB1)

## Voltage
PWP = 55

pinA15.high()

## Set channel 2 to zero
t3ch2.pulse_width_percent(0)

Motor1 = Motor(PWP,pinA15,pinB0,pinB1,tim3, t3ch1, t3ch2)

Motor.motor_on(Motor1)
print("Running")
pyb.delay(500)
print('speeding up')
Motor.motor_update(Motor1,100)
pyb.delay(500)
print('STOP')
Motor.motor_stop(Motor1)


