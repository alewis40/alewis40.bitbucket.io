'''
@file FSM_elevator.py

@brief This FSM controls a virtual Elevator with two buttons and two floors. 

@details The user has 2 buttons to go to floors 1 and 2 respectively. There is a sensor at each floor to report the elevator's position.
'''

from random import choice
import time

class Elevator:
    '''
    @brief      A finite state machine to control an elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1
    S1_MOVING_DOWN  = 1    
    
    ## Constant defining State 2
    S3_STOPPED_AT_BOTTOM = 2    
    
    ## Constant defining State 3
    S2_MOVING_UP      = 3    
    
    ## Constant defining State 4
    S4_STOPPED_AT_TOP     = 4
    
    
    def __init__(self, interval, First, Second, Button_1, Button_2, Motor):
        '''
        @brief            Creates an Elevator object.
        @param First      An object from class Button representing bottom position. 
        @param Second     An object from class Button representing top position. 
        @param Button_1   An object from class Button representing the floor 1 button.
        @param Button_2   An object from class Button representing the floor 2 button. 
        @param Motor      An object from class MotorDriver representing the elevator motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
       
        ## The button object used for the floor 1 button
        self.Button_1 = Button_1
        
        ## The button object used for the floor 2 button
        self.Button_2 = Button_2
        
        ## The button object used for the top position
        self.First = First
        
        ## The button object used for the top position
        self.Second = Second
        
        ## The motor object moving the elevator
        self.Motor = Motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                if (self.First.getButtonState() != 1):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.Down()
                    print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
                else:
                    self.transitionTo(self.S3_STOPPED_AT_BOTTOM)
                    self.Motor.Stop()
                    print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            
            elif(self.state == self.S1_MOVING_DOWN):
                # Run State 1 Code               
                if(self.First.getButtonState()):
                    self.transitionTo(self.S3_STOPPED_AT_BOTTOM)
                    self.Motor.Stop()
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S2_MOVING_UP):
                # Run State 2 Code
                if(self.Second.getButtonState()):
                    self.transitionTo(self.S4_STOPPED_AT_TOP)
                    self.Motor.Stop()
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S3_STOPPED_AT_BOTTOM):
                # Run State 3 Code
                if(self.Button_2.getButtonState()):
                    self.transitionTo(self.S2_MOVING_UP)
                    self.Motor.Up()
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S4_STOPPED_AT_TOP):
                # Run State 4 Code
                if(self.Button_1.getButtonState()):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.Down()
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
                
                      
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to move the elevator up and down.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Up(self):
        '''
        @brief Moves the elevator up
        '''
        print('Elevator moving up')
    
    def Down(self):
        '''
        @brief Moves the elevator down
        '''
        print('Elevator moving down')
    
    def Stop(self):
        '''
        @brief Stops the elevator
        '''
        print('Motor Stopped')



























