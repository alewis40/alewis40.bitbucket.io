'''
@file shares.py
@brief A container for all the inter-task variables
@author Charlie Refvem modified by Alexander Lewis
'''

## The command character sent from the user interface task to the cipher task
cmd     = None

## The response from the cipher task after encoding
resp    = None

##LED Frequency in Hz
freq = 0


Vm = 0

## Desired rotational velocity 
des_w = 0

## Desired position
des_pos = 0



