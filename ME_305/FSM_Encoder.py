# -*- coding: utf-8 -*-
"""


Created on Mon Oct 19

@author: Alexander Lewis
"""


import pyb
import utime





class Encoder:
    '''
    @brief      A finite state machine to virtual and real LEDs.
    @details    This class implements a finite state machine to control virtual and real LEDs on the Nucleo.
    '''
       
    ## Virtual Blinking and initialization state 
    S1_Init_Track = 1
    ## Sine wave state
    S2_zero_position = 2
    ## Sawtooth wave state
    S3_print_position = 3
    
    S4_print_delta = 4
    
    S5_command = 5
    
    
    
    
    def __init__(self, interval, pin1, pin2, timer):
        '''
        @brief      Creates an LED_wave, LED_saw, and Virtual_LED object.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S1_Init_Track
        
        self.timer = timer
        self.pin1 = pin1
        self.pin2 = pin2

        tim = pyb.Timer(timer)
        tim.init(prescaler = 0,period =0xFFFF)
      
        tim.channel(1,pin = self.pin1, mode = pyb.Timer.ENC_AB)
        tim.channel(2,pin = self.pin2, mode = pyb.Timer.ENC_AB)
        self.tim = tim

                   
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval     
              
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time
               
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        self.last_position = 0
        
        self.position = 0
    

        
        
        
        
        
        
        

        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
             
        ## This is alternates between blinking and sine wave until it has run for 30 seconds

         
        self.curr_time = utime.ticks_ms()
        if (self.curr_time > self.next_time):
            
            if (self.state == self.S1_Init_Track):
                current_position = self.update()
                self.state = self.S5_command
                delta = current_position - self.last_position
            self.next_time = utime.ticks_add(utime.ticks_ms(),50)
            
            while(self.curr_time < self.next_time):
                self.curr_time = utime.ticks_ms()
                if (self.state == self.S5_command):
                    
                    print('Please enter one of 3 lowercase commands')
                    print('z = zero position, p = position, d = delta')
                    command = str(input('Enter Here: '))
                    print(command)
                    if command:
                        
                        if (command == 'p'):
                            
                            print(current_position)
                            self.state == self.S1_Init_Track
                            self.last_position = current_position
                        elif (command == 'd'):
                            
                            print(delta)
                            self.state == self.S1_Init_Track
                            self.last_position = current_position
                        elif (command == 'z'):
                            
                            self.position = 0
                            current_position = 0
                            self.last_position = 0
                            self.state == self.S1_Init_Track
                            delta = self.get_delta(current_position,self.last_position)
                        else:
                            pass
                else:
                    pass
            self.next_time = utime.ticks_add(utime.ticks_ms(),50)
            self.state = self.S1_Init_Track
                    
                
                
                      
        
    def update(self):

        ticks = self.tim.counter()
        return ticks;
    
    
    def get_delta(self,current_position,last_position):
            
        delta = current_position - last_position
        self.position += delta
        return self.position, delta
        
        
        