# -*- coding: utf-8 -*-
"""
@file Lab5_main.py

This script blinks the requested LED at the inputted frequency 1-10 Hz
It also interfaces with the Lab5.aia app by calling the Frequency_Receiver function

Created on Mon November 3rd 13:40:20 2020

@author: Alexander Lewis
"""

from LED_driver import freq_blink
from Frequency_receiver import Frequency_Receiver
from pyb import UART
import shares

## This sets up the bluetooth connection
uart = UART(3,9600)


# Alternating between tasks over and over
while True:
    Frequency_Receiver(uart)
    freq_blink()