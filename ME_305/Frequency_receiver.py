"""
@file Frequency_receiver.py
@brief Receives inputs over bluetooth UART, converts to Hz, and sends to shares.freq
@date Created on Thu Nov 19 14:11:22 2020

@author: Alexander Lewis
"""

import pyb
from pyb import UART
import shares


def Frequency_Receiver(uart):
    '''
        @brief      Frequency_Receiver checks for incoming characters from Lab5.aia
        @details    Once any character is received, it is checked to see if it is 's', or 0-10 ascii equivalent. Then the incoming integer is converted into 0-10 Hz. This frequency value is put in the variable shares.freq so it can be refrenced by other scripts. 
        '''
    if uart.any() != 0:
         val_int = uart.readchar()
         if val_int == 83:
             shares.freq = 0

         elif val_int == 48:
                 shares.freq = 10
         elif val_int == 49:
                 shares.freq = 1
         elif val_int == 50:
                 shares.freq = 2
         elif val_int == 51:
                 shares.freq = 3
         elif val_int == 52:
                 shares.freq = 4
         elif val_int == 53:
                 shares.freq = 5
         elif val_int == 54:
                 shares.freq = 6
         elif val_int == 55:
                 shares.freq = 7                
         elif val_int == 56:
                 shares.freq = 8                 
         elif val_int == 57:
                 shares.freq = 9                  
                
         else:
                 pass
    else:
        pass

