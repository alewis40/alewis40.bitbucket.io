# -*- coding: utf-8 -*-
"""
@file main_page.py
@mainpage
@author Alexander Lewis

#This portfolio showcases each assignment completed during ME 305. 
#One of the main lessons from this class was to plan out code as much as possible. As the assignment complexity increased, it became more important to reference the task and finite state diagrams. 
#The thing I enjoyed most about this class was getting the bluetooth module up and running in Lab 5. It was very exciting to make an app and have it effectively communicate with the nucleo. 
#The most difficult part of these projects was working with the motors and encoders in lab 6/7. 

@section sec_Lab1 Lab 1
#  In Lab 1 code was created to find a number in the Fibonacci sequence. It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Fibonacci_Code.py
#  
#  Documentation link: https://alewis40.bitbucket.io/ME_305/html/Fibonacci__Code_8py.html

@section sec_Lab2 Lab 2 
# In Lab 2 code was created to blink a virtual LED while alternating between running a sine wave and a sawtooth wave on LED A5 on the Nucleo.
#The Finite State Machine can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/FSM_LED.py
# 
#   Documentation link: https://alewis40.bitbucket.io/ME_305/html/FSM__LED_8py.html
#
#The Main File can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/LED_main.py
# 
#   Documentation link: https://alewis40.bitbucket.io/ME_305/html/LED__main_8py.html
#
@section sec_Lab3 Lab 3
#  In Lab 3 a main file was made to run the Encoder file and the User Interface FSM. It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Encoder_main.py
#  
#   Documentation link: https://alewis40.bitbucket.io/ME_305/html/Encoder__main_8py.html
#
#  An Encoder task file was created to track the movement of an encoder with the Nucleo. It also is built to send responses back to requests for position, setting position to zero, and getting delta between positions. It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Encoder.py
#  
#   Documentation link: https://alewis40.bitbucket.io/ME_305/html/Encoder_8py.html
#
#  A User Interface FSM was created to continuously ask for user commands and send them to the Encoder class for a response. It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Encoder_User.py
#  
#   Documentation link: https://alewis40.bitbucket.io/ME_305/html/Encoder__User_8py.html
#
# A shares file made by Charlie Refvem was used to hold the commands and responses that moved between the Encoder and User Interface Task Files. It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/shares.py
#  
#   Documentation link: https://alewis40.bitbucket.io/ME_305/html/shares_8py.html
#
@section sec_Lab4 Lab 4
#  In Lab 4 a main file was made to run when the Nucleo turns on or resets. It runs the back end encoder data gathering program. It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Lab4main.py
#  This was initially called main.py so that it would run when the Nucleo starts. 
#  Documentation link: https://alewis40.bitbucket.io/ME_305/html/Lab4main_8py.html
#
#  This backend Finite State Machine runs on the Nucleo. It waits for a command from the UI to gather data, and then once it gets a command to stop it sends the data out. It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/data.py
#  
#   Documentation link: https://alewis40.bitbucket.io/ME_305/html/data_8py.html
#
#  This is the backend State Diagram: 
@image html Lab_4_data_State_Diagram.png
#
#
#
#  A User Interface FSM was created to continuously ask for user commands and send them to the data FSM if the command is 'g' for go or 's' for stop. 
#  Once the data is gathered, it is put into a csv file and graphed. It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Lab4_UI.py
#
#  This is the User interface State Diagram:
@image html Lab4_UI_state_diagram.png
#
#This is an example output graph:     
@image html Lab4_output_graph.png
#After improving my encoder code with help from Dr. Wade, the output becomes much smoother. This is the new encoder driver result(using the new encoders)
@image html improved_encoder.png
#This encoder driver is used in Lab 6. Its documentation can be found here: file:///C:/Users/Alexander%20Lewis/Documents/alewis40.bitbucket.io/ME_305/html/Encoder__driver_8py.html
#
#
@section sec_Lab5 Lab 5
#  In Lab 5, 4 python scripts and a MIT app inventor user interface were used. 
#  No FSMs were needed. Here is the task diagram: 
@image html Lab5_tasks.png
#
#  Lab5_main.py was the main file running on the nucleo. It alternated between calling the frequency receiver function and the blink function. It also sets up bluetooth connection.
#  Lab5_main.py can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Lab5_main.py
#  
#   Documentation link: https://alewis40.bitbucket.io/ME_305/html/Lab5__main_8py.html
#
#  Frequency_receiver.py checks the UART for any inputs once every time it is called. If there is an input matching on of its preset 0-10 hz commands, it set the new frequency to the correct value in shares.py.
#  Frequency_receiver.py can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Frequency_receiver.py
#  
#  Documentation link: https://alewis40.bitbucket.io/ME_305/html/Frequency__receiver_8py.html
#
#  LED_driver.py blinks LED A5 once at the current frequency set in shares.py.
#  LED_driver.py can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/LED_driver.py
#  
#   Documentation link: https://alewis40.bitbucket.io/ME_305/html/LED__driver_8py.html
#
#  Shares.py was created by Professor Refvem, and modified with a new shares.freq variable. It simply holds this integer. It gets updated by the frequency receiver and referenced by the LED driver.
#  Shares.py can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/shares.py
#  
#  Documentation link: https://alewis40.bitbucket.io/ME_305/html/shares_8py.html
#
#  The app sends different integers over BT UART to signal 1-10 Hz or stop.
#  The app created can be seen here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Lab5.aia
#
# This is what the user sees when connected and when not connected: 
@image html Connected_screenshot.jpg
@image html Disconnected_screenshot.jpg
#
#
@section sec_Notes Some notes on Lab 6 and Lab 7:
#   Despite considerable effort, unfortunately these labs did not function as desired in the end. Individually the motor driver, encoder driver, and closed loop files worked as intended.They all contain test code at the bottom of their scripts that has been commented out. Something about the interaction of the motor driver and the closed loop files seemed to zero out the encoder driver's ability to function. I suspect some hardware issue is playing a factor as well, because one of the motors will only spin after being manually spun after the code tells it to run. 
#   Because the interaction of Closed_loop.py and Motor_Driver.py zeroed out all encoder data, I was unable to create graphs showing the proportional control. With limited time and no multimeter available to troubleshoot with, I have tried to compensate for this by continuing to add documentation to my code. I have also been revising the code to try and make it more modular, thus reducing the complexity.  
#
@section sec_Lab6 Lab 6
# 
#   In Lab 6 a motor driver was created. It has 3 methods, to enable and disable the motor, and to adjust the pulse width percentage used to run the motor. 
#   Motor_Driver.py can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Motor_Driver.py
#   
#   Documentation link: https://alewis40.bitbucket.io/ME_305/html/Motor__Driver_8py.html
#
#   This is the state diagram for Lab 6: 
@image html Lab6_task_diagram.png 
# 
#   Lab_6_main.py continually updates desired speed, actual speed, and % error.
#   It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Lab_6_main.py
#   
#   Documentation link: file:///C:/Users/Alexander%20Lewis/Documents/alewis40.bitbucket.io/ME_305/html/Lab__6__main_8py.html
#   
#   Closed_loop.py uses the encoder driver data to calculate speed and Vm. 
#   It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Closed_loop.py 
#   
#   Documentation link: file:///C:/Users/Alexander%20Lewis/Documents/alewis40.bitbucket.io/ME_305/html/Closed__loop_8py.html
#
#   Encoder_driver.py updates the encoder position. 
#   It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Encoder_driver.py    
#   
#   Documentation link: file:///C:/Users/Alexander%20Lewis/Documents/alewis40.bitbucket.io/ME_305/html/Encoder__driver_8py.html
#
#   Task_User_6.py is actually the main program that runs on the Nucleo. It waits until it receives a character from the UI. Then it sends gathers data for 30 seconds. Then it sends that data to the UI.
#   Task_User_6.py includes a FSM and a small section where Lab_6_main.py is initialized and run alternate with the user task. 
#   This is the FSM diagram: 
@image html Task_User_6.png 
#   It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Task_User_6.py
#   
#   Documentation link: file:///C:/Users/Alexander%20Lewis/Documents/alewis40.bitbucket.io/ME_305/html/Task__User__6_8py.html
#
#   Lab6_UI.py allows the user to enter Kp which starts the data gather process. Then it recieves, processes, and plots the output. 
#   The data is all sent as strings, which Lab6_UI.py adds together into one big string. It then divides into the time, error, w_des, and w_actual lists by separating the string at '/'. Then those lists are further split by commas, and plotted against the time list. 
@image html 6UI.png
#   It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Lab6_UI.py
#   
#   Documentation link: file:///C:/Users/Alexander%20Lewis/Documents/alewis40.bitbucket.io/ME_305/html/Lab6__UI_8py.html
#
#
@section sec_Lab7 Lab 7
#
#   This lab uses modified code from lab 6. The main file was updated to adjust the desired velocity profile while the motor is running, and to use the position profile to test accuracy. 
#   Lab_7_main.py can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Lab_7_main.py
#   
#   Documentation link: file:///C:/Users/Alexander%20Lewis/Documents/alewis40.bitbucket.io/ME_305/html/Lab__7__main_8py.html
#
#   Task_User_7.py was modified to send J values along with the actual and real positions and speeds so that accuracy can be tracked throughout the trial. It was also changed to 15 seconds so that it would last the same length as there was velocity and position profiles to follow. 
#   It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Task_User_7.py
#
#   Documentation link: file:///C:/Users/Alexander%20Lewis/Documents/alewis40.bitbucket.io/ME_305/html/Task_User_7_8py.html
#   
#   Lab7_UI.py had to be altered to receive and plot these different values. 
#   It can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/Lab7_UI.py
#   
#   Documentation link: file:///C:/Users/Alexander%20Lewis/Documents/alewis40.bitbucket.io/ME_305/html/Lab7_UI_8py.html
@page Homework

@section HW1
# For HW 1, a Finite State Machine was made for a virtual elevator. 
@section FSM_Elevator
#The Finite State Machine can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/FSM_elevator.py
# 
#   Documentation link: https://alewis40.bitbucket.io/ME_305/html/FSM__elevator_8py.html
@section main_Elevator
The code to run the Finite State Machine can be found here: https://bitbucket.org/alewis40/alewis40.bitbucket.io/src/master/ME_305/main_Elevator.py
# 
#   Documentation link: https://alewis40.bitbucket.io/ME_305/html/main__Elevator_8py.html
#This is the state diagram for the elevator.     
@image html Elevator_State_Diagram.png



"""

