# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 11:16:58 2020

@author: Alexander Lewis
"""

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

## Setup Values
X0 = .1

def ODE(X0):
    xdot0 = 0
    k = 2
    alpha = 5
    deltat = .01
    t = 0
    
    fxn = -k*X0 - alpha*(X0**3)

    xn = X0
    xdotn = xdot0

    time = [0]
    data = [xn]
    xn1 = xn + xdotn*deltat + .5*fxn*deltat**2
    fxn1 = -k*xn1 - alpha*xn1**3

    while (t < 10):
    
        xn1 = xn + xdotn*deltat + .5*fxn*(deltat**2)
    
        xdotn1 = xdotn + .5*(fxn + fxn1)*deltat
    
    
    
        fxn = fxn1
        xn = xn1
        xdotn = xdotn1
        t += deltat
        fxn1 = -k*xn1 - alpha*xn1**3
        time.append(t)
        data.append(xn)
        
    return time, data



a = ODE(0)
b = ODE(.1)
c = ODE(.2)
d = ODE(.3)
e = ODE(.4)
f = ODE(.5)
g = ODE(.6)
h = ODE(.7)
i = ODE(.8)
j = ODE(.9)
k = ODE(1)


# plot the data
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(a[0], a[1])
ax.plot(b[0], b[1])
ax.plot(c[0], c[1])
ax.plot(d[0], d[1])
ax.plot(e[0], e[1])
ax.plot(f[0], f[1])
ax.plot(g[0], g[1])
ax.plot(h[0], h[1])
ax.plot(i[0], i[1])
ax.plot(j[0], j[1])
ax.plot(k[0], k[1])

    
    
    
   
    

