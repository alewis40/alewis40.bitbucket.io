# -*- coding: utf-8 -*-
"""
Created on Sat Oct 10 22:13:58 2020

@author: Alexander Lewis
"""

import pyb

pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
n = 0
for n in range(5):
    pinA5.high()
    pyb.delay(1000)
    pinA5.low()
    pyb.delay(1000)
    
machine.sleep()

    