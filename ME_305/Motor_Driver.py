"""
@file Motor_Driver.py
@brief This script drives the motor at the given voltage.
@details motor_enable() and motor_disable() activate and deactivate the sleep pin respectively. motor_set_duty() allows for the pulse width percentage to be modified.
@date Created on Tue Nov 10 14:16:00 2020

@author: Alexander Lewis
"""
import pyb
class Motor:
    def __init__(self):
        '''@brief Sets up motor and timer parameters'''
        ##initialize all the motor information
        self.sleep_pin = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)
        self.pwm1 = pyb.Pin(pyb.Pin.cpu.B0)
        self.pwm2 = pyb.Pin(pyb.Pin.cpu.B1)
        self.timer = pyb.Timer(3,freq = 20000)
        self.channel1 = self.timer.channel(3,pyb.Timer.PWM,pin=self.pwm1)
        self.channel2 = self.timer.channel(4,pyb.Timer.PWM,pin = self.pwm2)
        
        ## sets high pin to low until motor should be on
        self.sleep_pin.low()

        self.channel1.pulse_width_percent(0)
        self.channel2.pulse_width_percent(0)
        
    def motor_enable(self):
        '''@brief Turns the sleep pin to high '''
        self.sleep_pin.high()
    
    def motor_set_duty(self,percentage):
        '''
        @brief     updates the pulse width percentage
        '''
        if (percentage < 100):
            self.channel2.pulse_width_percent(percentage)
        elif (percentage > 100):
            self.channel2.pulse_width_percent(100)
        else:
            pass

    
    def motor_disable(self):
        '''
        @brief     Turns the sleep pin off
        '''
        self.sleep_pin.low()

#test code: 
# motor1 = Motor()
# motor1.motor_enable()
# motor1.motor_set_duty(60)
# pyb.delay(1000)
# motor1.motor_set_duty(100)
# pyb.delay(1000)
# motor1.motor_disable()

