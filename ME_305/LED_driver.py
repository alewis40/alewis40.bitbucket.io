"""
@file LED_driver.py
@brief This file contains the LED freq_blink function. It blinks LED A5 once at the frequency set in shares.py 
@date Created on Mon November 3rd 13:40:20 2020

@author: Alexander Lewis
"""


import pyb
import shares

    
def freq_blink():
    '''
        @brief     freq_blink does one blink of LED A5 the frequency in Hz in shares.freq
        '''
    frequency = int(shares.freq)
    
    Pin = pyb.Pin (pyb.Pin.cpu.A5,pyb.Pin.OUT_PP)
    if frequency != 0:
        Pin.high()
        pyb.delay(int(1000/(2*frequency)))
        Pin.low()
        pyb.delay(int(1000/(2*frequency)))
    else:
        Pin.low()
    
