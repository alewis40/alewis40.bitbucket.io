# -*- coding: utf-8 -*-
"""
@file Encoder_driver.py
@brief This encoder object calculates the encoder position. 
@date Created on Mon Nov 23 10:20:53 2020

@author: Alexander Lewis
"""

import pyb
import utime


class Encoder:
    def __init__(self, pin1, pin2, timer,dbg=True):
        '''
        Creates an encoder object
        @param Timer identifying which timer will be used on the Nucleo
        @param pin1 identifies the first encoder pin
        @param pin2 identifies the second encoder pin
        
        '''
        ## The state to run on the next iteration of the task.
    
        self.dbg = dbg
        
        self.timer = timer
        self.pin1 = pin1
        self.pin2 = pin2
        
        ## assigns the timer on the Nucleo
        self.tim = pyb.Timer(timer)
        ## sets the prescaler so that it catches every tick, and sets the period
        self.tim.init(prescaler = 0,period =0xFFFF)
        
        ## sets the channels      
        self.tim.channel(1,pin = self.pin1, mode = pyb.Timer.ENC_AB)
        self.tim.channel(2,pin = self.pin2, mode = pyb.Timer.ENC_AB)
        
        ## Initialize count, delta, and position at zero
        self._count = 0
        self._delta = 0
        self._position = 0 
        self._last_count = 0
        self._last_delta = 0
        
        self._DELTAMAX = 0xFFFF/2
        self._DELTAMIN = -0xFFFF/2    

        self._dir = 1
        self.CCW = 0 
        self._PER = 0xFFFF
        self.start = utime.ticks_ms()
        self.end = self.start
     
    def update(self,IRQsource=None):
        '''
        @brief Updates the encoder position by checking if delta is good and then adding it to position
        @details Made by Professor Wade and heavily modified by Alex Lewis
        '''
        self.start = self.end 
        self._last_count = self._count
        self._last_delta = self._delta
        self._count = self.tim.counter()
        self._delta = self._count - self._last_count
        
       
        if (self._delta>self._DELTAMAX):
            self._delta = self._delta - self._PER
        elif (self._delta<self._DELTAMIN):
            self._delta = self._delta + self._PER
       
        if (self._dir == self.CCW):
            self._delta = - self._delta
       
        self._position = self._position + self._delta
        self.end = utime.ticks_ms()
        return self._position
        
    def delta(self):
        '''
        @brief Prints the current delta
        '''
        
        return self._delta
    
    def get_position(self):
        '''
        @brief Prints the current position
        '''        
        return self._position
        
# #Testing and creating better graph
# timer = 4
# pin1 = pyb.Pin.cpu.B6
# pin2 = pyb.Pin.cpu.B7

# encoder = Encoder(pin1,pin2,timer,dbg = True)

# while True:
#     print(encoder.update())
    
    
    
    
    
# # #n = 0
# # # time =[] 
# # # position = []
# # # while n < 100:
# #     time.append(n)
#     pyb.delay(10)
#     encoder.get_position()
#     position.append(encoder.get_position())
#     n +=1
# print('Time:')
# print(time)
# print("Position:")
# print(position)



    




