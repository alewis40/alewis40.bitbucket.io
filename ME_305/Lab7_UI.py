# -*- coding: utf-8 -*-
"""
@file Lab7_UI.py
@brief This script runs on the computer and sends Kp inputed by the user over the UART to the Lab 7 Task_User_7.py file. It then receives data after 15 seconds and graphs it.
@detail The updated version receives and plots J, as well as actual and real speed and position. 
Created on Fri Oct 23 21:17:24 2020

@author: Alexander Lewis
"""
import time

import serial
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import xlsxwriter



ser = serial.Serial(port='COM7',baudrate=115273,timeout=1)



class Lab6_UI:
    
    S0_waiting = 0
    S1_plotting = 1
    

    def __init__(self, interval):
        self.state = self.S0_waiting
        
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        self.curr_time = self.start_time
        
        ## Initialize variables
        self.time = []
        self.des_pos = []
        self.pos_actual = []
        self.w_des = []
        self.w_act = []
        self.J = []
        self.list_all =""
        self.split_list= []
        
    def run(self):
        ''' @brief This file sets the inputted Kp, and gathers data for 30 seconds. It then plots the data it gathered. '''
        
        self.curr_time = time.time()
    
        if self.curr_time > self.next_time:
          
            if(self.state == self.S0_waiting):
                print("Waiting to gather data. Enter an integer to set Kp and start data gathering.")
                self.user_command()
                self.transitionTo(self.S1_plotting)
                

                            
            elif(self.state == self.S1_plotting):
                ## The incoming strings are all added into one giant string. At this point, the numbers are in the order of time, error, w_actual, w_des
                if (ser.readline().decode('ascii') != None):
                    self.list_all= self.list_all + (ser.readline().decode('ascii'))
                else:
                    ## Divides the giant string into a list where each item in the list is a string of all time values, all error values etc.
                    self.split_list = self.list_all.split('/')
                    
                    ## The following 6 lines split the 6 strings left into lists
                    self.time = self.split_list[0].split(',')
                    self.des_pos = self.split_list[1].split(',')
                    self.pos_actual = self.split_list[2].split(',')
                    self.w_des = self.split_list[3].split(',')
                    self.w_act = self.split_list[4].split(',')
                    self.J = self.split_list[5].split(',')
                    
                    ## Time, error, w_actual, w_des are graphed
                    self.plot_data(self.time, self.des_pos , self.pos_actual,self.w_des, self.w_act,self.J)
                    
                    ## Transition back to waiting
                    self.transitionTo(self.S0_waiting)
     
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval


    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

            
    
    def user_command(self):
        inv = input('Enter integer for Kp here: ')
        ser.write(inv).encode('ascii')
        

    
        
    def plot_data(self,time,a,b,c,d,e):
        print('plotting')

        fig, ax = plt.subplots()
        ax.plot(time, a)
        ax.set(xlabel='time', ylabel='desired position')
        title=('desired position vs time')
        ax.grid()

        fig.savefig("Lab7_desired_position")
        plt.show()

        
        fig, ax = plt.subplots()
        ax.plot(time, b)
        ax.set(xlabel='time', ylabel='actual position')
        title=('actual position vs time')
        ax.grid()

        fig.savefig("Lab7_J_error")
        plt.show()
        
        fig, ax = plt.subplots()
        ax.plot(time, c)
        ax.set(xlabel='time', ylabel='desired speed')
        title=('desired speed vs time')
        ax.grid()

        fig.savefig("Lab7_J_error")
        plt.show()

        fig, ax = plt.subplots()
        ax.plot(time, d)
        ax.set(xlabel='time', ylabel='actual speed')
        title=('actual speed vs time')
        ax.grid()

        fig.savefig("actual speed vs time")
        plt.show()

        fig, ax = plt.subplots()
        ax.plot(time, e)
        ax.set(xlabel='time', ylabel='J error')
        title=('J error vs time')
        ax.grid()

        fig.savefig("Lab7_J_error")
        plt.show()

UI = Lab6_UI(.1)
while True:
       
    UI.run()