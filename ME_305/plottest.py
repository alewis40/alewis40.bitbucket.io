# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 11:29:53 2020

@author: Alexander Lewis
"""

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

time = [1,2,3,4,5,6,7]
position = [1,3,4,6,8,8,8]

fig, ax = plt.subplots()
ax.plot(time, position)
ax.set(xlabel='time (s)', ylabel='voltage (mV)',
       title='About as simple as it gets, folks')
ax.grid()

fig.savefig("test.png")
plt.show()