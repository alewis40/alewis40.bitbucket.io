# -*- coding: utf-8 -*-
"""
@file Lab5_LED_Driver.py

This script blinks the requested LED at the inputted frequency 1-10 Hz

Created on Mon November 3rd 13:40:20 2020

@author: Alexander Lewis
"""


import math
import pyb
import utime
from pyb import UART
import shares




class Lab5_UI:
    '''
    @brief      A finite state machine that blinks an LED at a frequency sent from the phone.
    '''   
    
    def __init__(self, interval):
        '''
        @brief      Creates an LED_wave, LED_saw, and Virtual_LED object.
        '''
             
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval     
              
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
               
        ## Counter that describes the number of times the task has run
        self.runs = 0
        ## Sets up the UART
        self.myuart = UART(3,9600)
        ## Sets up Pin A5
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5,pyb.Pin.OUT_PP)
        ## Sets frequency in Hz
        self.frequency = 1
        
        
        

        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        print('running')
        self.curr_time = utime.ticks_us()
        #print('outside loop')
        if (self.curr_time - self.next_time >= 0):
            #print('inside loop')
            
            # Specifying the next time the task will run
            self.next_time = (self.next_time) + (self.interval)
            
                if self.myuart.any() != 0:
                    self.myuart.readline() == shares.freq
            
            
        
            
        
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
    
    
        
task = Lab5_UI(.1)
while True:
       
    task.run()