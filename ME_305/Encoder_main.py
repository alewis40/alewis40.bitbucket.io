# -*- coding: utf-8 -*-
"""
@file Encoder_main.py
@brief This script is the main file for the UI and encoder tasks.
@details This script prompts the user to enter commands delta, position, or zero position. Then it calls the encoder and user interface tasks.
Created on Mon Oct 19 2020
@author: Alexander Lewis
"""

from Encoder import Encoder
from Encoder_User import TaskUser
import pyb


## User interface task, works with serial port
task0 = TaskUser  (0, 1000, dbg=False) 

timer = 3
pin1 = pyb.Pin.cpu.A6
pin2 = pyb.Pin.cpu.A7

task1 = Encoder(1000, pin1, pin2, timer,dbg=False)

taskList = [task0,
            task1]
#Running the task over and over

## Initial Instructions 
print("Please enter 'd' for delta, 'p' for position, or 'z' to zero position")
while True:
    for task in taskList:
        task.run()


