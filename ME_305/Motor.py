# -*- coding: utf-8 -*-
"""
@file Motor.py
@brief This script drives the motor at the given voltage.
Created on Tue Nov 10 14:16:00 2020

@author: Alexander Lewis
"""

class Motor:
    def __init__(self,voltage,highpin,pwm1,pwm2,timer,channel1,channel2):
        
        ##initialize all the motor information
        self.voltage = voltage
        self.highpin = highpin
        self.pwm1 = pwm1
        self.pwm2 = pwm2
        self.timer = timer
        self.channel1 = channel1
        self.channel2 = channel2
        
        ## sets high pin to low until motor should be on
        self.highpin.low()

        self.channel1.pulse_width_percent(0)
        self.channel2.pulse_width_percent(0)
        
    def motor_on(self):
        self.highpin.high()
        self.channel2.pulse_width_percent(self.voltage)
    
    def motor_stop(self):
        self.channel2.pulse_width_percent(0)

