# -*- coding: utf-8 -*-
"""
@file Lab_7_main.py
@brief Alternates between the Closed_loop.py and updating the motor speed. This updated version continually updates the desired velocity to control the speed. It also updates the desired position to calculate J error. 
Created on Tue Nov 24 16:51:46 2020

@author: Alexander Lewis
"""

import pyb
from Closed_loop import ClosedLoop
from Motor_Driver import Motor
import shares
from pyb import UART
import utime
import velocity_profile

myuart = UART(2)

class Lab_6_main:
    
    def __init__(self):
        
        ## Set initial Kp
        self.gain = 1
        
        ## Set initial desired speed in rpm
        self.des_w = 100
        
        ## Sets  initial duty cycle %
        shares.des_w = self.des_w
        
        ## Sets up motor
        self.Motor1 = Motor()
        self.Motor1.motor_disable()
            
        # ## Sets up loop
        self.Loop1 = ClosedLoop(self.gain)

        ## Enable motor
        self.Motor1.motor_enable()
        
        
        ## Set Kp
        self.Loop1.set_kp(self.gain)


        self._start_time = utime.ticks_ms()
        
        ## This variable is added so that the velocity profile knows what time it is. 
        self.time = utime.ticks_ms() - self._start_time
        
        ## k = total iterations of Lab_7_main.run()
        self.k = 1
        
        ## J error
        self.J = 0
        
        

        
    def run(self):
        '''@brief This continually updates the error, the duty cycle, and the gain. 
           @detail The data doesn't have to be saved in a list. because each value is being sent continuously, the user interface will read it when it is in the plotting mode. '''
        ## Sets up loop
        Loop1 = ClosedLoop(shares.gain)
        
        ## Adjusts Motor duty percentage
        self.Motor1.motor_set_duty(shares.Vm)
        
        ## Updates the actual speed
        Loop1.update()
        
        ## Calculates error
        error = (shares.des_w - self.Loop1._w_actual)/self.des_w
        
        ## Calculates time
        time = utime.ticks_ms() - self._start_time 
        
        ## Updates shares.des_w
        self.velocity_profile()
        
        ## Updates shares.des_pos
        self.position_profile()
        
        ## Adds to J
        self.J_error_calc()
        
        
        
    
    def velocity_profile(self):
        '''@brief This method sets the shares.des_w to the new value based on time. '''
        ## Time is set each run
        self.time = utime.ticks_ms() - self._start_time
        
        ## Converts time to seconds from milliseconds
        self.time = self.time / 1000
        
        i = 0
        ## Iterates to find the current row of the dataset.Then sets the current desired speed based on column 2 of the correct row. Starting i from 0 each time is slower, but it reduces complexity since i doesn't need to be reset at the end of the list. 
        while i in velocity_profile.velocity_profile[0]:
            if velocity_profile.velocity_profile[0][i] > self.time:
                shares.des_w = velocity_profile.velocity_profile[1][i]
                break
            i += 1
            
            
    def position_profile(self):
        '''@brief This method sets the shares.des_pos to the new value based on time. shares.des_pos is not used to modulate the motor, but it is used to calculate J error.'''
        
        ## Time is set each run
        self.time = utime.ticks_ms() - self._start_time
        
        ## Converts time to seconds from milliseconds
        self.time = self.time / 1000
        
        i = 0
        ## Iterates to find the current row of the dataset.Then sets the current desired position based on column 3 of the correct row. Starting i from 0 each time is slower, but it reduces complexity since i doesn't need to be reset at the end of the list. 
        while i in velocity_profile.velocity_profile[0]:
            if velocity_profile.velocity_profile[0][i] > self.time:
                ## Must convert rpm to deg/s
                shares.des_pos = (velocity_profile.velocity_profile[2][i])* 360/60
                break
            i += 1
        
            
    def J_error_calc(self):
        '''@brief This method calculates J using desired position, desired speed, actual speed, and actual position. 
           @detail Each time it is called, this method calculates J delta and adds it to the growing sum.'''
        
        J_delta = (1/self.k)*((shares.des_w-self.Loop1._w_actual)^2+(shares.des_pos-self.Loop1.pos1)^2)
        self.J += J_delta
        
        
    

#nucleo = Lab_6_main()


#while True:
#    nucleo.run()

