# -*- coding: utf-8 -*-
"""
@file main.py
Created on Fri Oct 23 21:50:00 2020

@author: Alexander Lewis
"""

from data import Encoder_data
import pyb


timer = 3
pin1 = pyb.Pin.cpu.A6
pin2 = pyb.Pin.cpu.A7

task = Encoder_data(100000, pin1, pin2, timer,dbg=False)


while True:
    task.run()