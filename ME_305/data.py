# -*- coding: utf-8 -*-
"""
@file data.py
@brief This script tracks and gathers data on the encoder position. 
@details When the go command is received, this script starts tracking both time and position. When the stop command is receieved these lists are send back to the User Interface through the UART. 
Created on Mon Oct 26 15:47:43 2020

@author: Alexander Lewis
"""
import pyb
import utime
from pyb import UART


class Encoder_data:
    '''
    @brief      A class to track the position of an encoder.
    '''
    ## Initialization state
    S0_INIT       = 0
    
    ## Tracking Position
    S1_Tracking   = 1
    
    ## Waiting 
    S2_Waiting = 2
    
    def __init__(self, interval, pin1, pin2, timer,dbg=True):
        '''
        Creates an encoder object
        @param Timer identifying which timer will be used on the Nucleo
        @param pin1 identifies the first encoder pin
        @param pin2 identifies the second encoder pin
        
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        self.dbg = dbg
        
        self.timer = timer
        self.pin1 = pin1
        self.pin2 = pin2
        
        ## assigns the timer on the Nucleo
        self.tim = pyb.Timer(timer)
        ## sets the prescaler so that it catches every tick, and sets the period
        self.tim.init(prescaler = 0,period =0xFFFF)
        
        ## sets the channels      
        self.tim.channel(1,pin = self.pin1, mode = pyb.Timer.ENC_AB)
        self.tim.channel(2,pin = self.pin2, mode = pyb.Timer.ENC_AB)
                           
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval)     
              
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Sets last_ticks for the first run
        self.last_ticks = 0
        
        ## Sets time when counting starts
        self.zero_time = utime.ticks_ms()
        ## Sets initial position
        self.position = 0
        
        self.myuart = UART(2)
        
        self.Time = [0]
        self.position_data = []
        self.curr_ticks = self.last_ticks
        
        
        
        


        
    def run(self):
        
        '''
        @brief      Runs one iteration of the task
        ''' 
        self.curr_time = utime.ticks_us()
        curr_ticks = 0
        #print('outside loop')
        if (self.curr_time - self.next_time >= 0):
            #print('inside loop')
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
            ##Run Initialization State
            
            if (self.state == self.S0_INIT):
                self.transitionTo(self.S2_Waiting)
                


                        
            
            ##Collects data and waits for stop command  
            
            elif (self.state == self.S1_Tracking):
                time = (utime.ticks_ms() - self.zero_time)
                curr_ticks = self.update()
                delta = self.delta(curr_ticks)
                self.last_ticks = curr_ticks
                position = self.get_position(delta)
                
                
                position_data = position
                
                self.Time.append(time)   
                
                
                self.position_data.append(position_data)
                
                
                
                
                
                
                if self.myuart.any() != 0:
                    
                    self.myuart.readchar() 
                    
                    i = 0
                    while (i <  len(self.Time)) and (i < len(self.position_data)):
                        #self.myuart.write(self.Time[i] + ',' + self.position_data[i])
                        print(str(self.Time[i]) + ','+ str(self.position_data[i]))
                        i += 1 
                    print('')
                    print('done')
            
                    self.transitionTo(self.S2_Waiting)
                     
                    
                    
                   

            ##Tracks position, and waits for go command    
            elif (self.state == self.S2_Waiting):         
                
                if self.myuart.any() != 0:
                    self.myuart.readchar()
                       
                        ## Zero out the past data and start tracking new positional data
                    self.Time = [0]
                    self.set_position(0)
                    self.last_ticks = 0
                    self.position_data = [0]
                    self.transitionTo(self.S1_Tracking)
                    self.zero_time = utime.ticks_ms()
                    
                else:
                    pass
                        
                    
                    

                    
            else:
                # Invalid state code (error handling)
                pass
        
            
                
                
                
        self.curr_time = utime.ticks_us()        
            
            
            
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)

     
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
    
    def delta(self,curr_ticks):
        '''
        Finds the change in ticks from the last iteration
        '''
        if (self.last_ticks == 0):
            delta = 0
        else:
            delta = curr_ticks - self.last_ticks
        return delta

        
    def update(self):
        '''
        Updates current number of ticks
        '''        
        ticks = self.tim.counter()
        return ticks;
    
    def set_position(self, pos_entered):
        '''
        Enters new position
        '''        
        self.position = pos_entered
        
            

    def get_position(self, Delta):
        '''
        Adds delta to the running position tally
        '''        
        self.position += Delta
        return self.position
    
    def UI(self,cmd):
        '''
        Gives different outputs for different User input commands
        '''

            
        if (cmd == 100):
            resp = self.delta(self.update())
            print(resp)
            
        elif (cmd == 112):
            resp = self.get_position(self.delta(self.update()))
            print(resp)
            
        elif (cmd == 122):
            resp = self.set_position(0)
            print(resp)
          
        
        else:
            
            resp = cmd
            
        return resp
    
    

    
        

    
            
        
        
        
        