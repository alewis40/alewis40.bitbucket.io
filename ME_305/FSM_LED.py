# -*- coding: utf-8 -*-
"""
@file FSM_LED.py

This file blinks a virtual LED while alternating LED A5 on the Nucleo between a sine wave and sawtooth wave pattern.

Created on Mon Oct 12 13:40:20 2020

@author: Alexander Lewis
"""


import math
import pyb
import utime





class LED:
    '''
    @brief      A finite state machine to virtual and real LEDs.
    @details    This class implements a finite state machine to control virtual and real LEDs on the Nucleo.
    '''
       
    ## Virtual Blinking and initialization state 
    S0_BLINK = 0
    ## Sine wave state
    S1_WAVE = 1
    ## Sawtooth wave state
    S2_SAW = 2
    
    
    
    
    def __init__(self, interval, LED_wave, LED_saw, Virtual_LED):
        '''
        @brief      Creates an LED_wave, LED_saw, and Virtual_LED object.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_BLINK
        
        ## Stores wave object
        self.LED_wave = LED_wave
        
        ## Stores saw object
        self.LED_saw = LED_saw
        
        ## Stores Virtual LED object
        self.Virtual_LED = Virtual_LED
             
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval     
              
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
               
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        
        

        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
        ## Sets a start time for the cycle. The decision to switch between sine and sawtooth waves is controlled by this time.
        self.cycle_start_time = utime.ticks_ms()
        
        ## This is alternates between blinking and sine wave until it has run for 30 seconds
        print("WAVE ON")
        self.state == self.S0_BLINK
        while (utime.ticks_ms() - self.cycle_start_time < 30000):
            self.curr_time = utime.ticks_ms()
            if self.curr_time > self.next_time:
                
                if (self.state == self.S0_BLINK):
                    self.state = self.S1_WAVE
                    self.LED_wave.wave()
        
                elif (self.state == self.S1_WAVE):
                    self.state = self.S0_BLINK
                    self.Virtual_LED.blink()
            
        
        self.cycle_start_time = utime.ticks_ms()
        self.state = self.S0_BLINK
        
        ##This is alternates between blinking and sawtooth wave until it has run for 30 seconds
        print('SAW ON')
        while (utime.ticks_ms() - self.cycle_start_time < 30000):
            
            self.curr_time = utime.ticks_ms()
            if self.curr_time > self.next_time:
                
                if (self.state == self.S0_BLINK):
                    self.state = self.S2_SAW
                    self.LED_saw.saw()
                    
                elif (self.state == self.S2_SAW):
                    self.state = self.S0_BLINK
                    self.Virtual_LED.blink()
        ## Updating Time          
        self.next_time = utime.ticks_add(utime.ticks_ms(),100)
        print('current time = ' + str(self.curr_time))
               
                    
            
            
            

class Virtual:
    '''
    @brief      A Virtual LED class
    @details    This class represents a Virtual LED that blinks continuously.
    '''
   
    ## LED ON state
    ON = 0
    ## LED OFF state
    OFF = 1
    start = utime.ticks_ms()

    def __init__(self): 
        '''
        @brief      Creates a Virtual LED object
        @details    The LED is set to ON initially.
        '''
            ## The initial LED state
        self.state = self.ON           
            ## This variable allows the LED to measure how long it has been ON or OFF. This is key to timing when the LED should change. 
        last_time = 0
        self.last_time = last_time 
            
            
    def blink(self):
            ## This is the current time at the moment blink() is called. 
            time = utime.ticks_ms()
            
            ## If 500 milliseconds have passed and the LED is on, the LED is turned off. 
            if (self.state == self.ON) and (time - self.last_time >=500):
                print("LED OFF")
                self.state = self.OFF
                self.last_time = utime.ticks_ms()
            
            ## If 500 milliseconds have passed and the LED is off, the LED is turned on. 
            elif (self.state == self.OFF) and (time - self.last_time >=500):  
                print("LED ON")
                self.state = self.ON
                self.last_time = utime.ticks_ms()
            else:
                
                pass
            
          
                    
              
class Wave: 
    '''
    @brief      A sine wave class
    @details    This class makes a sine wave with the A5 pin on the Nucleo.
    '''
    def __init__(self):
        '''
        @brief Creates a Sine wave LED Object
        '''       
        pass
    
    def wave(self):
        ## Identifies pin on Nucleo
        pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        ## Sets timer for PWM
        tim1 = pyb.Timer(2, freq = 20000)
        t2ch1 = tim1.channel(1,pyb.Timer.PWM,pin=pinA5)
        ## Sets pulse width percentage from 0 - 100% using the sine function.
        t2ch1.pulse_width_percent(100*math.sin(utime.ticks_ms()/1000))
        

class Saw:
    '''
    @brief      A sawtooth wave class
    @details    This class makes a sawtooth wave with the A5 pin on the Nucleo.
    '''
    
    def __init__(self):
        '''
        @brief Creates a Sawtooth wave LED Object
        '''
        pass
    def saw(self):
        ## Identifies pin on Nucleo
        pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        ## Sets timer for PWM
        tim1 = pyb.Timer(2, freq = 20000)
        t2ch1 = tim1.channel(1,pyb.Timer.PWM,pin=pinA5)
        ## Sets pulse width percentage from 0- 100% using the Fourier expansion of the sawtooth wave. 
        t2ch1.pulse_width_percent((100*2/math.pi)*(math.sin(math.pi*utime.ticks_ms()/1000)-1/2*math.sin(2*math.pi*utime.ticks_ms()/1000)+1/3*math.sin(3*math.pi*utime.ticks_ms()/1000)-1/4*math.sin(4*math.pi*utime.ticks_ms()/1000)))
        
        
        
       
      