var classClosed__loop_1_1ClosedLoop =
[
    [ "__init__", "classClosed__loop_1_1ClosedLoop.html#a3af2c32d24c7a944e056c472a01764f6", null ],
    [ "get_kp", "classClosed__loop_1_1ClosedLoop.html#a49dd5c2edd9e12a408c57189d2a16a05", null ],
    [ "set_kp", "classClosed__loop_1_1ClosedLoop.html#a7a8bf5262335c1d82f7d90d8baf9bbe5", null ],
    [ "update", "classClosed__loop_1_1ClosedLoop.html#ade4e4f9a681bbc8d443066dfc2c3c002", null ],
    [ "w_actual", "classClosed__loop_1_1ClosedLoop.html#a52be4a243b9e9d5732c621a993e478a8", null ],
    [ "encoder", "classClosed__loop_1_1ClosedLoop.html#a6f0b99ee0cc91cd64e8e847254cf77f6", null ],
    [ "end", "classClosed__loop_1_1ClosedLoop.html#ab98af709696fb096cc2afa0056251b1e", null ],
    [ "pin1", "classClosed__loop_1_1ClosedLoop.html#a0e650d4b9a4ee56b45b1057d7fb0ea25", null ],
    [ "pin2", "classClosed__loop_1_1ClosedLoop.html#a6d17db3b53bb5ad6a1d182b3abe91e4d", null ],
    [ "pos1", "classClosed__loop_1_1ClosedLoop.html#ae2a575c6f2363a5adb9b64eb0f25c440", null ],
    [ "start", "classClosed__loop_1_1ClosedLoop.html#ac80158516e1074c7ce2f5140833d15f6", null ],
    [ "timer", "classClosed__loop_1_1ClosedLoop.html#afeafc6037e4b017f5e2c190a9a56db6e", null ]
];