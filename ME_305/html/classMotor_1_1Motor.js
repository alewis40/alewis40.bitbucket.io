var classMotor_1_1Motor =
[
    [ "__init__", "classMotor_1_1Motor.html#a8e8e88d0d5e9a57264d618c1727ad0c7", null ],
    [ "motor_on", "classMotor_1_1Motor.html#a05dcab9cf9e9318f7bde27e7e3a2b151", null ],
    [ "motor_stop", "classMotor_1_1Motor.html#a8beec0ae8564ac2029eb4be5a093fa99", null ],
    [ "channel1", "classMotor_1_1Motor.html#a090eaebb541b91ccf94fae77833d4d78", null ],
    [ "channel2", "classMotor_1_1Motor.html#af031a7c2131b1ff1db0d7f9427db6f23", null ],
    [ "highpin", "classMotor_1_1Motor.html#a3d26f36eaddb557e9291c831f95a2c38", null ],
    [ "pwm1", "classMotor_1_1Motor.html#aeb5ff8254b1c244ebeaf244bd5d3387e", null ],
    [ "pwm2", "classMotor_1_1Motor.html#a03bb5dbf25c73938b69717bea94fd01c", null ],
    [ "timer", "classMotor_1_1Motor.html#aaeddc0be738c3d5ee1b9ff10893aa6de", null ],
    [ "voltage", "classMotor_1_1Motor.html#aaba260c69f6ff71b71d3545d309392a4", null ]
];