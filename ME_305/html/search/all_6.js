var searchData=
[
  ['gain_32',['gain',['../classLab__6__main_1_1Lab__6__main.html#acf2b0758cc6946cf2b10ef2c0d4fb945',1,'Lab_6_main.Lab_6_main.gain()'],['../classLab__7__main_1_1Lab__6__main.html#a769639d7f739b1f70f4eeb3e93ce6118',1,'Lab_7_main.Lab_6_main.gain()']]],
  ['gather_5fdata_33',['Gather_Data',['../classTask__User__6_1_1Gather__Data.html',1,'Task_User_6.Gather_Data'],['../classTask__User__7_1_1Gather__Data.html',1,'Task_User_7.Gather_Data']]],
  ['get_5fkp_34',['get_kp',['../classClosed__loop_1_1ClosedLoop.html#a49dd5c2edd9e12a408c57189d2a16a05',1,'Closed_loop::ClosedLoop']]],
  ['get_5fposition_35',['get_position',['../classdata_1_1Encoder__data.html#a96f49ad81a547b442c31238f890ad3dc',1,'data.Encoder_data.get_position()'],['../classEncoder_1_1Encoder.html#a53a97b8dc668164d0b1764273cdf0fc2',1,'Encoder.Encoder.get_position()'],['../classEncoder__driver_1_1Encoder.html#a50f4b1f91e097e92e35c6cf14fcbc79e',1,'Encoder_driver.Encoder.get_position()']]],
  ['getbuttonstate_36',['getButtonState',['../classFSM__elevator_1_1Button.html#ae7c44f3c522680b5a0f10361e45d07bc',1,'FSM_elevator::Button']]]
];
