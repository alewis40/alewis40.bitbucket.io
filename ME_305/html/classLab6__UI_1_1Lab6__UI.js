var classLab6__UI_1_1Lab6__UI =
[
    [ "__init__", "classLab6__UI_1_1Lab6__UI.html#ab439fa994082cd2793f86ed82652a765", null ],
    [ "plot_data", "classLab6__UI_1_1Lab6__UI.html#a6bdaf906b8f0f1d7ca0bc2950d5e3119", null ],
    [ "run", "classLab6__UI_1_1Lab6__UI.html#a0a763931c2863760fc6d4fe7b2095804", null ],
    [ "transitionTo", "classLab6__UI_1_1Lab6__UI.html#af29b5b3ba51bb7a67016ed3796096da3", null ],
    [ "user_command", "classLab6__UI_1_1Lab6__UI.html#af04d13d3b2b42139942f5ff80c2db057", null ],
    [ "curr_time", "classLab6__UI_1_1Lab6__UI.html#a369fef6c0e2d15b7a2be4570a9600e9b", null ],
    [ "error", "classLab6__UI_1_1Lab6__UI.html#a5ffd6c7a6f7911d29533b8738215aaf2", null ],
    [ "interval", "classLab6__UI_1_1Lab6__UI.html#abbc171de645829b06ab2385dc0d83da6", null ],
    [ "list_all", "classLab6__UI_1_1Lab6__UI.html#aa80f82dfad0f65e430afeeb790e2d296", null ],
    [ "next_time", "classLab6__UI_1_1Lab6__UI.html#aed90ffec1f34e7ef0a3c45941f7b2e61", null ],
    [ "split_list", "classLab6__UI_1_1Lab6__UI.html#af6dab50b22ec69ead74d8f614f05ee7e", null ],
    [ "start_time", "classLab6__UI_1_1Lab6__UI.html#afb708c7b058efcd42fb0bf1cdd2b4949", null ],
    [ "state", "classLab6__UI_1_1Lab6__UI.html#abbe19534dd322f1ab32afe1087bb7a48", null ],
    [ "time", "classLab6__UI_1_1Lab6__UI.html#a949d20667a2cad5e5b6bbdd41a47ca37", null ],
    [ "w_actual", "classLab6__UI_1_1Lab6__UI.html#ac4f8bcd1f84447576c21e21008c8a522", null ],
    [ "w_des", "classLab6__UI_1_1Lab6__UI.html#a782b084c9b937a57dc40803d1e8bd381", null ]
];