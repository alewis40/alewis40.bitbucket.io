/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 305", "index.html", [
    [ "Lab 1", "index.html#sec_Lab1", null ],
    [ "Lab 2", "index.html#sec_Lab2", null ],
    [ "Lab 3", "index.html#sec_Lab3", null ],
    [ "Lab 4", "index.html#sec_Lab4", null ],
    [ "Lab 5", "index.html#sec_Lab5", null ],
    [ "Some notes on Lab 6 and Lab 7:", "index.html#sec_Notes", null ],
    [ "Lab 6", "index.html#sec_Lab6", null ],
    [ "Lab 7", "index.html#sec_Lab7", null ],
    [ "Homework", "Homework.html", [
      [ "HW1", "Homework.html#HW1", null ],
      [ "FSM_Elevator", "Homework.html#FSM_Elevator", null ],
      [ "main_Elevator", "Homework.html#main_Elevator", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Closed__loop_8py.html",
"classLab__6__main_1_1Lab__6__main.html#a7ffbe81adace12d99e05931a6582b982"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';