var annotated_dup =
[
    [ "Closed_loop", null, [
      [ "ClosedLoop", "classClosed__loop_1_1ClosedLoop.html", "classClosed__loop_1_1ClosedLoop" ]
    ] ],
    [ "data", null, [
      [ "Encoder_data", "classdata_1_1Encoder__data.html", "classdata_1_1Encoder__data" ]
    ] ],
    [ "Encoder", null, [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "Encoder_driver", null, [
      [ "Encoder", "classEncoder__driver_1_1Encoder.html", "classEncoder__driver_1_1Encoder" ]
    ] ],
    [ "Encoder_User", null, [
      [ "TaskUser", "classEncoder__User_1_1TaskUser.html", "classEncoder__User_1_1TaskUser" ]
    ] ],
    [ "FSM_elevator", null, [
      [ "Button", "classFSM__elevator_1_1Button.html", "classFSM__elevator_1_1Button" ],
      [ "Elevator", "classFSM__elevator_1_1Elevator.html", "classFSM__elevator_1_1Elevator" ],
      [ "MotorDriver", "classFSM__elevator_1_1MotorDriver.html", "classFSM__elevator_1_1MotorDriver" ]
    ] ],
    [ "FSM_Encoder", null, [
      [ "Encoder", "classFSM__Encoder_1_1Encoder.html", "classFSM__Encoder_1_1Encoder" ]
    ] ],
    [ "FSM_LED", null, [
      [ "LED", "classFSM__LED_1_1LED.html", "classFSM__LED_1_1LED" ],
      [ "Saw", "classFSM__LED_1_1Saw.html", "classFSM__LED_1_1Saw" ],
      [ "Virtual", "classFSM__LED_1_1Virtual.html", "classFSM__LED_1_1Virtual" ],
      [ "Wave", "classFSM__LED_1_1Wave.html", "classFSM__LED_1_1Wave" ]
    ] ],
    [ "Lab4_UI", null, [
      [ "Lab4_UI", "classLab4__UI_1_1Lab4__UI.html", "classLab4__UI_1_1Lab4__UI" ]
    ] ],
    [ "Lab5_FSM", null, [
      [ "Lab5_UI", "classLab5__FSM_1_1Lab5__UI.html", "classLab5__FSM_1_1Lab5__UI" ]
    ] ],
    [ "Lab6_UI", null, [
      [ "Lab6_UI", "classLab6__UI_1_1Lab6__UI.html", "classLab6__UI_1_1Lab6__UI" ]
    ] ],
    [ "Lab7_UI", null, [
      [ "Lab6_UI", "classLab7__UI_1_1Lab6__UI.html", "classLab7__UI_1_1Lab6__UI" ]
    ] ],
    [ "Lab_6_main", null, [
      [ "Lab_6_main", "classLab__6__main_1_1Lab__6__main.html", "classLab__6__main_1_1Lab__6__main" ]
    ] ],
    [ "Lab_7_main", null, [
      [ "Lab_6_main", "classLab__7__main_1_1Lab__6__main.html", "classLab__7__main_1_1Lab__6__main" ]
    ] ],
    [ "Motor", null, [
      [ "Motor", "classMotor_1_1Motor.html", "classMotor_1_1Motor" ]
    ] ],
    [ "Motor_Driver", null, [
      [ "Motor", "classMotor__Driver_1_1Motor.html", "classMotor__Driver_1_1Motor" ]
    ] ],
    [ "Task_User_6", null, [
      [ "Gather_Data", "classTask__User__6_1_1Gather__Data.html", "classTask__User__6_1_1Gather__Data" ]
    ] ],
    [ "Task_User_7", null, [
      [ "Gather_Data", "classTask__User__7_1_1Gather__Data.html", "classTask__User__7_1_1Gather__Data" ]
    ] ]
];