var classLab4__UI_1_1Lab4__UI =
[
    [ "__init__", "classLab4__UI_1_1Lab4__UI.html#a52b4acbb12c85b2ef181cf3a0f1163da", null ],
    [ "request_data", "classLab4__UI_1_1Lab4__UI.html#a9165120c894000e8ca86266b583ad0a1", null ],
    [ "run", "classLab4__UI_1_1Lab4__UI.html#a7b4a7c3bc7abe6028b737160699d5e96", null ],
    [ "sendGo", "classLab4__UI_1_1Lab4__UI.html#a239f37e0c485e025cb9c2eba808fdf9c", null ],
    [ "sendStop", "classLab4__UI_1_1Lab4__UI.html#ade2da0bc47069d8a49b75854f1aeee90", null ],
    [ "transitionTo", "classLab4__UI_1_1Lab4__UI.html#a3c886da48b347965a4588dc41cd0754d", null ],
    [ "curr_time", "classLab4__UI_1_1Lab4__UI.html#a0565799a971fc16c42802fcdc527fff0", null ],
    [ "interval", "classLab4__UI_1_1Lab4__UI.html#a435caa15e20c1eb5b621f9d5377a9340", null ],
    [ "next_time", "classLab4__UI_1_1Lab4__UI.html#a9bbfd036a558135def249f5c26ac8014", null ],
    [ "position", "classLab4__UI_1_1Lab4__UI.html#a05cd3078927db82d7f5d1bf6f087e62e", null ],
    [ "start_time", "classLab4__UI_1_1Lab4__UI.html#ac83b136d402d9d01d4076c3edbbadccc", null ],
    [ "state", "classLab4__UI_1_1Lab4__UI.html#a404d80b7c154c390f3ec2cae7ea5499e", null ],
    [ "time", "classLab4__UI_1_1Lab4__UI.html#a775f5e585647f50276c3360a1efa05a4", null ]
];