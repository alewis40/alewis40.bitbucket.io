var classLab6__UI_1_1Lab4__UI =
[
    [ "__init__", "classLab6__UI_1_1Lab4__UI.html#a2dc25143a06a7e6dda6c550750b1e0b8", null ],
    [ "plot_data", "classLab6__UI_1_1Lab4__UI.html#a2fda5d7ce262fec87b321de6a30da6be", null ],
    [ "run", "classLab6__UI_1_1Lab4__UI.html#ac761fa7660b7fb0f0d25ee7d119efef2", null ],
    [ "transitionTo", "classLab6__UI_1_1Lab4__UI.html#a299bb763eeecc7babe16e07eedbd4eaa", null ],
    [ "user_command", "classLab6__UI_1_1Lab4__UI.html#a7bdb197106a05dc04af5c9e5c0243667", null ],
    [ "curr_time", "classLab6__UI_1_1Lab4__UI.html#a0206e25936f3001ad7c9ed70346f71d2", null ],
    [ "error", "classLab6__UI_1_1Lab4__UI.html#a51ac5065ca9dd69da35138efc648c24a", null ],
    [ "interval", "classLab6__UI_1_1Lab4__UI.html#a366ae67f49e7ae471b683a504e410981", null ],
    [ "next_time", "classLab6__UI_1_1Lab4__UI.html#a3da930ecef2c87e5bdcfd70730f26218", null ],
    [ "start_time", "classLab6__UI_1_1Lab4__UI.html#abd623f7aea59604713ae91a54447ab49", null ],
    [ "state", "classLab6__UI_1_1Lab4__UI.html#a3c0738d823a576f880be351fb1cf898f", null ],
    [ "time", "classLab6__UI_1_1Lab4__UI.html#a659d0c3abf1be0f2d8e215591014b6d7", null ],
    [ "w_actual", "classLab6__UI_1_1Lab4__UI.html#a5b3f35b3d219e55efcd07de106f5ec93", null ],
    [ "w_des", "classLab6__UI_1_1Lab4__UI.html#aea92a6b4ee8b66db36bc4878c13575be", null ]
];