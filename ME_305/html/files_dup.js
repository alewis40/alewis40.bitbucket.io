var files_dup =
[
    [ "Closed_loop.py", "Closed__loop_8py.html", [
      [ "ClosedLoop", "classClosed__loop_1_1ClosedLoop.html", "classClosed__loop_1_1ClosedLoop" ]
    ] ],
    [ "data.py", "data_8py.html", [
      [ "Encoder_data", "classdata_1_1Encoder__data.html", "classdata_1_1Encoder__data" ]
    ] ],
    [ "Encoder.py", "Encoder_8py.html", [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "Encoder_driver.py", "Encoder__driver_8py.html", [
      [ "Encoder", "classEncoder__driver_1_1Encoder.html", "classEncoder__driver_1_1Encoder" ]
    ] ],
    [ "Encoder_main.py", "Encoder__main_8py.html", "Encoder__main_8py" ],
    [ "Encoder_User.py", "Encoder__User_8py.html", [
      [ "TaskUser", "classEncoder__User_1_1TaskUser.html", "classEncoder__User_1_1TaskUser" ]
    ] ],
    [ "Fibonacci_Code.py", "Fibonacci__Code_8py.html", "Fibonacci__Code_8py" ],
    [ "Frequency_receiver.py", "Frequency__receiver_8py.html", "Frequency__receiver_8py" ],
    [ "FSM_elevator.py", "FSM__elevator_8py.html", [
      [ "Elevator", "classFSM__elevator_1_1Elevator.html", "classFSM__elevator_1_1Elevator" ],
      [ "Button", "classFSM__elevator_1_1Button.html", "classFSM__elevator_1_1Button" ],
      [ "MotorDriver", "classFSM__elevator_1_1MotorDriver.html", "classFSM__elevator_1_1MotorDriver" ]
    ] ],
    [ "FSM_LED.py", "FSM__LED_8py.html", [
      [ "LED", "classFSM__LED_1_1LED.html", "classFSM__LED_1_1LED" ],
      [ "Virtual", "classFSM__LED_1_1Virtual.html", "classFSM__LED_1_1Virtual" ],
      [ "Wave", "classFSM__LED_1_1Wave.html", "classFSM__LED_1_1Wave" ],
      [ "Saw", "classFSM__LED_1_1Saw.html", "classFSM__LED_1_1Saw" ]
    ] ],
    [ "Lab4_UI.py", "Lab4__UI_8py.html", "Lab4__UI_8py" ],
    [ "Lab4main.py", "Lab4main_8py.html", "Lab4main_8py" ],
    [ "Lab5_main.py", "Lab5__main_8py.html", "Lab5__main_8py" ],
    [ "Lab6_UI.py", "Lab6__UI_8py.html", "Lab6__UI_8py" ],
    [ "Lab7_UI.py", "Lab7__UI_8py.html", "Lab7__UI_8py" ],
    [ "Lab_6_main.py", "Lab__6__main_8py.html", "Lab__6__main_8py" ],
    [ "Lab_7_main.py", "Lab__7__main_8py.html", "Lab__7__main_8py" ],
    [ "LED_driver.py", "LED__driver_8py.html", "LED__driver_8py" ],
    [ "LED_main.py", "LED__main_8py.html", "LED__main_8py" ],
    [ "main_Elevator.py", "main__Elevator_8py.html", "main__Elevator_8py" ],
    [ "Motor.py", "Motor_8py.html", [
      [ "Motor", "classMotor_1_1Motor.html", "classMotor_1_1Motor" ]
    ] ],
    [ "Motor_Driver.py", "Motor__Driver_8py.html", [
      [ "Motor", "classMotor__Driver_1_1Motor.html", "classMotor__Driver_1_1Motor" ]
    ] ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "Task_User_6.py", "Task__User__6_8py.html", "Task__User__6_8py" ],
    [ "Task_User_7.py", "Task__User__7_8py.html", "Task__User__7_8py" ],
    [ "velocity_profile.py", "velocity__profile_8py.html", "velocity__profile_8py" ]
];