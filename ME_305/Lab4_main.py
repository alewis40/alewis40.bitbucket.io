# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 21:50:00 2020

@author: Alexander Lewis
"""

from Lab4_UI import Lab4_UI
from main import Encoder_data
import pyb


interval = .1
Task0 = Lab4_UI(interval)

timer = 3
pin1 = pyb.Pin.cpu.A6
pin2 = pyb.Pin.cpu.A7

Task1 = Encoder_data(1000, pin1, pin2, timer,dbg=False)
taskList = [Task0,
            Task1]

while True:
    for task in taskList:
        task.run()