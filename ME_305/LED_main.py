# -*- coding: utf-8 -*-
"""
@file LED_main.py
@brief This script runs the virtual LED and LED A5 by initializing and running FSM_LED

Created on Mon Oct 12 14:33:50 2020

@author: Alexander Lewis
"""

from FSM_LED import LED, Virtual, Wave, Saw

# Creating a sine wave, sawtooth, and virtual LED object
LED_wave = Wave()
LED_saw = Saw()
Virtual_LED = Virtual()


# Creating a task using the objects above and an interval time. 

task1 = LED(.01,LED_wave,LED_saw,Virtual_LED)

#Running the task over and over
for n in range(1000):
    task1.run()


