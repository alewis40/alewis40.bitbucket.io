# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 13:11:41 2020

@author: Alexander Lewis

This program runs with the BTHC05connect.aia app
"""
import pyb
from pyb import UART

pinA5 = pyb.Pin (pyb.Pin.cpu.A5,pyb.Pin.OUT_PP)
uart = UART(3,9600)

while True:
     if uart.any() != 0:
         val = int(uart.readline())
         if val == 0:
             print(val,' turns it OFF')
             pinA5.low()
         elif val == 1:
             print(val,' turns it ON')
             pinA5.high()
         else:
            print(val)