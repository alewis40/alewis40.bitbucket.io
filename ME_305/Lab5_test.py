# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 13:11:41 2020

@author: Alexander Lewis
"""
#x = 5

#x = isinstance(x,int)

#print(x)

#y = "Hi"

#y = isinstance(y,int)

#print(y)


import pyb
from pyb import UART

pinA5 = pyb.Pin (pyb.Pin.cpu.A5,pyb.Pin.OUT_PP)
uart = UART(3,9600)

while True:
     if uart.any() != 0:
         val_int = uart.readchar()
         if val_int == 83:
             #pinA5.low()
             print('STOP turns it OFF')

         elif val_int == 48:
                 print('10 hz')
                 pinA5.high()
         elif val_int == 49:
                 print('1 hz')
                 pinA5.high()
         elif val_int == 50:
                 print('2 hz')
                 pinA5.high()
         elif val_int == 51:
                 print('3 hz')
                 pinA5.high()
         elif val_int == 52:
                 print('4 hz')
                 pinA5.high()
         elif val_int == 53:
                 print('5 hz')
                 pinA5.high()
         elif val_int == 54:
                 print('6 hz')
                 pinA5.high()
         elif val_int == 55:
                 print('7 hz')
                 pinA5.high()                 
         elif val_int == 56:
                 print('8 hz')
                 pinA5.high()                 
         elif val_int == 57:
                 print('9 hz')
                 pinA5.high()                  
                
         else:
                 print(val_int)